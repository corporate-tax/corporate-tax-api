# corporate-tax-api

## Introduction

This project provides REST webservices for calculates the tax amount of a company type based on its annual sales
revenues.

The calculation is made by a **Calculator**.

Many companies types may have the same Calculator, but configured differently.

By using existing Calculator, but by changing the config, new company types can be supported without a new deployment.

## REST API

Cf. `https://{domain}/v3/api-docs/`

```
POST /tax/{companyType}

Body:
{
    "annualSalesRevenues": 0
}

Response:
{
    "taxAmount": 0
}
```

## Calculators configuration

The calculators' configuration is for the momement in this file: `application/src/main/resources/calculators-config.yml`
.

One entry by company type, with its calculator, and the calculator configuration.

```yaml
AUTO_ENTREPRISE:
  calculatorName: percentageTaxCalculator
  rate: 25
```

For the moment, the calculatorName is the name of a Spring bean.

## Security

For the moment, the API is secured by a basic authentication:

```
username: user
password: Q!J=6xud_LVb2za=QNwP!yzURcRR8YnpM&Nt_@BGN4SwS%6Kt2mJe3LnA55AXJJg
```