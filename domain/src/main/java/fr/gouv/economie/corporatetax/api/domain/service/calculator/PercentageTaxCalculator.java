package fr.gouv.economie.corporatetax.api.domain.service.calculator;

import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PercentageTaxCalculator implements TaxCalculator<PercentageTaxCalculatorConfig> {

    public static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

    @Override
    public BigDecimal calculate(TaxCalculationRequest request, PercentageTaxCalculatorConfig config) {
        return request.getAnnualSalesRevenues().multiply(config.getRate()).divide(ONE_HUNDRED, 2, RoundingMode.CEILING);
    }
}
