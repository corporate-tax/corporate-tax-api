package fr.gouv.economie.corporatetax.api.domain.service.calculator;

import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PercentageTaxCalculatorConfig extends CompanyTaxCalculatorConfig {

    private BigDecimal rate;

}
