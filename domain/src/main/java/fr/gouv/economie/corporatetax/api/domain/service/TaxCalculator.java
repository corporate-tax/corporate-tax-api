package fr.gouv.economie.corporatetax.api.domain.service;

import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;

import java.math.BigDecimal;

@FunctionalInterface
public interface TaxCalculator<E extends CompanyTaxCalculatorConfig> {

    BigDecimal calculate(TaxCalculationRequest request, E config);
}
