package fr.gouv.economie.corporatetax.api.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class TaxCalculationResult {

    private BigDecimal taxAmount;

}
