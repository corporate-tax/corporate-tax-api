package fr.gouv.economie.corporatetax.api.domain.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TaxCalculationRequest {

    @NotNull
    private BigDecimal annualSalesRevenues;

}
