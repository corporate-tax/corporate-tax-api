package fr.gouv.economie.corporatetax.api.domain.service;

import fr.gouv.economie.corporatetax.api.domain.exception.UnsupportedCompanyTypeException;
import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationResult;
import fr.gouv.economie.corporatetax.api.domain.repository.TaxCalculatorConfigRepository;
import lombok.AllArgsConstructor;

import javax.inject.Named;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Named
@AllArgsConstructor
public class TaxCalculatorService {

    private final TaxCalculatorConfigRepository taxCalculatorConfigRepository;
    private final TaxCalculatorFactory taxCalculatorFactory;

    public TaxCalculationResult calculate(String companyType, TaxCalculationRequest request) {
        Optional<CompanyTaxCalculatorConfig> configO = taxCalculatorConfigRepository.findConfigByCompanyType(companyType);

        if (configO.isEmpty()) {
            throw new UnsupportedCompanyTypeException(companyType);
        }

        CompanyTaxCalculatorConfig config = configO.get();

        TaxCalculator<CompanyTaxCalculatorConfig> taxCalculator = taxCalculatorFactory.getCalculator(config.getCalculatorName());
        BigDecimal taxAmount = taxCalculator.calculate(request, config);

        // Rounding for more money ^^
        return new TaxCalculationResult(taxAmount.setScale(0, RoundingMode.CEILING));
    }
}
