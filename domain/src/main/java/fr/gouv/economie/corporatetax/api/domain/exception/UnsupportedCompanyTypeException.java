package fr.gouv.economie.corporatetax.api.domain.exception;

public class UnsupportedCompanyTypeException extends RuntimeException {

    public UnsupportedCompanyTypeException(String type) {
        super("Unsupported company type: " + type);
    }
}
