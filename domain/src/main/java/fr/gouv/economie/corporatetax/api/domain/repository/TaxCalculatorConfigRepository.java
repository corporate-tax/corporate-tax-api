package fr.gouv.economie.corporatetax.api.domain.repository;

import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;

import java.util.Optional;

public interface TaxCalculatorConfigRepository {

    Optional<CompanyTaxCalculatorConfig> findConfigByCompanyType(String companyType);
}
