package fr.gouv.economie.corporatetax.api.domain.exception;

public class UnknownTaxCalculatorException extends RuntimeException {

    public UnknownTaxCalculatorException(String taxCalculatorName, Throwable cause) {
        super("Unknown tax calculator: " + taxCalculatorName, cause);
    }
}

