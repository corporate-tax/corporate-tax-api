package fr.gouv.economie.corporatetax.api.domain.service;

import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;

public interface TaxCalculatorFactory {

    TaxCalculator<CompanyTaxCalculatorConfig> getCalculator(String calculatorName);
}
