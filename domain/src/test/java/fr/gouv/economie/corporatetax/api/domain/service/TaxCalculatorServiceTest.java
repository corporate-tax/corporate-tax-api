package fr.gouv.economie.corporatetax.api.domain.service;

import fr.gouv.economie.corporatetax.api.domain.exception.UnsupportedCompanyTypeException;
import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationResult;
import fr.gouv.economie.corporatetax.api.domain.repository.TaxCalculatorConfigRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TaxCalculatorServiceTest {

    private static final String DEFAULT_COMPANY_TYPE = "TEST";

    @InjectMocks
    TaxCalculatorService taxCalculatorService;

    @Mock
    private TaxCalculatorConfigRepository taxCalculatorConfigRepository;
    @Mock
    private TaxCalculatorFactory taxCalculatorFactory;

    @Test
    void calculate() {
        // TaxCalculatorConfigRepository
        CompanyTaxCalculatorConfig companyTaxCalculatorConfig = new CompanyTaxCalculatorConfig("testTaxCalculator");
        when(taxCalculatorConfigRepository.findConfigByCompanyType(DEFAULT_COMPANY_TYPE)).thenReturn(Optional.of(companyTaxCalculatorConfig));

        // TaxCalculatorFactory
        when(taxCalculatorFactory.getCalculator(eq(companyTaxCalculatorConfig.getCalculatorName()))).thenReturn((request, config) -> BigDecimal.ONE);

        TaxCalculationResult result = taxCalculatorService.calculate(DEFAULT_COMPANY_TYPE, new TaxCalculationRequest(BigDecimal.TEN));
        assertThat(result.getTaxAmount()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    void calculate_rounding() {
        // TaxCalculatorConfigRepository
        CompanyTaxCalculatorConfig companyTaxCalculatorConfig = new CompanyTaxCalculatorConfig("testTaxCalculator");
        when(taxCalculatorConfigRepository.findConfigByCompanyType(DEFAULT_COMPANY_TYPE)).thenReturn(Optional.of(companyTaxCalculatorConfig));

        // TaxCalculatorFactory
        when(taxCalculatorFactory.getCalculator(eq(companyTaxCalculatorConfig.getCalculatorName()))).thenReturn((request, config) -> new BigDecimal("2.63"));

        TaxCalculationResult result = taxCalculatorService.calculate(DEFAULT_COMPANY_TYPE, new TaxCalculationRequest(BigDecimal.TEN));
        assertThat(result.getTaxAmount()).isEqualTo(new BigDecimal("3"));
    }

    @Test
    void calculate_unsupportedCompanyType() {
        String companyType = "UNKNOWN_COMPANY_TYPE";

        // TaxCalculatorConfigRepository
        when(taxCalculatorConfigRepository.findConfigByCompanyType(companyType)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> taxCalculatorService.calculate(companyType, new TaxCalculationRequest(BigDecimal.TEN)))
                .isInstanceOf(UnsupportedCompanyTypeException.class)
                .hasMessage(new UnsupportedCompanyTypeException(companyType).getMessage());
    }
}