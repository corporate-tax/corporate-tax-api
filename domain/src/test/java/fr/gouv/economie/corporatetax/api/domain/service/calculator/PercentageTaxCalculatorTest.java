package fr.gouv.economie.corporatetax.api.domain.service.calculator;

import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class PercentageTaxCalculatorTest {

    private final PercentageTaxCalculator percentageTaxCalculator = new PercentageTaxCalculator();

    @Test
    void calculate() {
        PercentageTaxCalculatorConfig config = new PercentageTaxCalculatorConfig(new BigDecimal("21"));
        BigDecimal result = percentageTaxCalculator.calculate(new TaxCalculationRequest(BigDecimal.TEN), config);

        assertThat(result).isEqualTo(new BigDecimal("2.10"));
    }

    @Test
    void calculate_rounding() {
        PercentageTaxCalculatorConfig config = new PercentageTaxCalculatorConfig(new BigDecimal("21.3145"));
        BigDecimal result = percentageTaxCalculator.calculate(new TaxCalculationRequest(BigDecimal.TEN), config);

        assertThat(result).isEqualTo(new BigDecimal("2.14"));
    }
}