package fr.gouv.economie.corporatetax.api.application.controller;

import fr.gouv.economie.corporatetax.api.application.common.error.ErrorResponse;
import fr.gouv.economie.corporatetax.api.domain.exception.UnsupportedCompanyTypeException;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationResult;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculatorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tax")
@AllArgsConstructor
public class TaxCalculationController {

    private final TaxCalculatorService taxCalculatorService;

    @PostMapping("/{companyType}")
    public TaxCalculationResult calculate(@PathVariable String companyType, @RequestBody @Valid TaxCalculationRequest request) {
        return taxCalculatorService.calculate(companyType, request);
    }

    @ExceptionHandler(UnsupportedCompanyTypeException.class)
    public ResponseEntity<ErrorResponse> handleUnsupportedCompanyTypeException(UnsupportedCompanyTypeException exception) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(exception.getMessage()));
    }
}
