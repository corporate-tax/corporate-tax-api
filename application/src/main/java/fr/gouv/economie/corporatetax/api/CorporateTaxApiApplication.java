package fr.gouv.economie.corporatetax.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorporateTaxApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CorporateTaxApiApplication.class, args);
    }

}
