package fr.gouv.economie.corporatetax.api.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.gouv.economie.corporatetax.api.application.common.error.ErrorResponse;
import fr.gouv.economie.corporatetax.api.domain.exception.UnsupportedCompanyTypeException;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationRequest;
import fr.gouv.economie.corporatetax.api.domain.model.TaxCalculationResult;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TaxCalculationController.class)
class TaxCalculationControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaxCalculatorService taxCalculatorService;

    @Test
    @WithMockUser("55c87815-000f-4f7c-8fd1-f6baab08d0a8")
    void calculate() throws Exception {
        String companyType = "AUTO_ENTREPRISE";

        TaxCalculationRequest request = new TaxCalculationRequest(new BigDecimal("12255.0"));
        TaxCalculationResult response = new TaxCalculationResult(BigDecimal.ONE);
        when(taxCalculatorService.calculate(eq(companyType), eq(request))).thenReturn(response);

        mockMvc.perform(post("/tax/{companyType}", companyType)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                )
                .andExpect(status().isOk())
                .andExpect(content().string((is(objectMapper.writeValueAsString(response)))));
    }

    @Test
    @WithMockUser("55c87815-000f-4f7c-8fd1-f6baab08d0a8")
    void calculate_unsupportedCompanyType() throws Exception {
        String companyType = "UNSUPPORTED";

        TaxCalculationRequest request = new TaxCalculationRequest(new BigDecimal("12255.0"));
        UnsupportedCompanyTypeException exception = new UnsupportedCompanyTypeException(companyType);
        when(taxCalculatorService.calculate(eq(companyType), eq(request))).thenThrow(exception);

        mockMvc.perform(post("/tax/{companyType}", companyType)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
                )
                .andExpect(status().isBadRequest())
                .andExpect(content().string((is(objectMapper.writeValueAsString(new ErrorResponse(exception.getMessage()))))));
    }
}
