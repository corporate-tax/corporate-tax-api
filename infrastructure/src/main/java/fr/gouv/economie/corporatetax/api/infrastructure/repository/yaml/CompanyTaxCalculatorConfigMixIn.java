package fr.gouv.economie.corporatetax.api.infrastructure.repository.yaml;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import fr.gouv.economie.corporatetax.api.domain.service.calculator.PercentageTaxCalculatorConfig;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, include = PROPERTY, property = "calculatorName", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = PercentageTaxCalculatorConfig.class, name = "percentageTaxCalculator")
})
public class CompanyTaxCalculatorConfigMixIn {
}
