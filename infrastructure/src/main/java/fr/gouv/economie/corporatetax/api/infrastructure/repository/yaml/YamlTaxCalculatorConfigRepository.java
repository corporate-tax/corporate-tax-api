package fr.gouv.economie.corporatetax.api.infrastructure.repository.yaml;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.repository.TaxCalculatorConfigRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class YamlTaxCalculatorConfigRepository implements TaxCalculatorConfigRepository {

    private final String yamlConfigUri;
    private final ObjectMapper psg;

    public YamlTaxCalculatorConfigRepository(@Value("${calculator.config.repository.yml.uri}") String yamlConfigUri) {
        this.yamlConfigUri = yamlConfigUri;

        psg = new ObjectMapper(new YAMLFactory());
        psg.addMixIn(CompanyTaxCalculatorConfig.class, CompanyTaxCalculatorConfigMixIn.class);
    }

    @Override
    public Optional<CompanyTaxCalculatorConfig> findConfigByCompanyType(String companyType) {
        var typeRef = new TypeReference<HashMap<String, CompanyTaxCalculatorConfig>>() {
        };

        try (InputStream yamlIs = getClass().getClassLoader().getResourceAsStream(yamlConfigUri)) {
            if (yamlIs == null) {
                throw new YamlConfigLoadingException(yamlConfigUri, new FileNotFoundException());
            }

            Map<String, CompanyTaxCalculatorConfig> map = psg.readValue(yamlIs, typeRef);

            return Optional.ofNullable(map.get(companyType));
        } catch (IOException e) {
            throw new YamlConfigLoadingException(yamlConfigUri, e);
        }
    }

}
