package fr.gouv.economie.corporatetax.api.infrastructure.repository.yaml;

import java.io.IOException;

public class YamlConfigLoadingException extends RuntimeException {

    public YamlConfigLoadingException(String yamlConfigUri, IOException e) {
        super("Unable to load the yaml config uri: " + yamlConfigUri, e);
    }
}
