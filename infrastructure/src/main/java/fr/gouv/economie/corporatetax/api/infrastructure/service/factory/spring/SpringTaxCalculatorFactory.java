package fr.gouv.economie.corporatetax.api.infrastructure.service.factory.spring;

import fr.gouv.economie.corporatetax.api.domain.exception.UnknownTaxCalculatorException;
import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculator;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculatorFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringTaxCalculatorFactory implements TaxCalculatorFactory, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public TaxCalculator<CompanyTaxCalculatorConfig> getCalculator(String calculatorName) {
        try {
            return applicationContext.getBean(calculatorName, TaxCalculator.class);
        }
        catch (NoSuchBeanDefinitionException e) {
            throw new UnknownTaxCalculatorException(calculatorName, e);
        }
    }
}
