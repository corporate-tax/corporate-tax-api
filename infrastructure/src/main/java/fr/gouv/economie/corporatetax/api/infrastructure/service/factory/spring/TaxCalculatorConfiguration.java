package fr.gouv.economie.corporatetax.api.infrastructure.service.factory.spring;

import fr.gouv.economie.corporatetax.api.domain.service.calculator.PercentageTaxCalculator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TaxCalculatorConfiguration {

    @Bean
    public PercentageTaxCalculator percentageTaxCalculator() {
        return new PercentageTaxCalculator();
    }

}
