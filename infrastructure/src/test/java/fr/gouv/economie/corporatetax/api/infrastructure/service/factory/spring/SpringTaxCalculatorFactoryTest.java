package fr.gouv.economie.corporatetax.api.infrastructure.service.factory.spring;

import fr.gouv.economie.corporatetax.api.domain.exception.UnknownTaxCalculatorException;
import fr.gouv.economie.corporatetax.api.domain.model.CompanyTaxCalculatorConfig;
import fr.gouv.economie.corporatetax.api.domain.service.TaxCalculator;
import fr.gouv.economie.corporatetax.api.domain.service.calculator.PercentageTaxCalculatorConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest(classes = SpringTaxCalculatorFactoryTest.TestApplication.class)
class SpringTaxCalculatorFactoryTest {

    @Autowired
    private SpringTaxCalculatorFactory springTaxCalculatorFactory;

    @Test
    void getCalculator() {
        TaxCalculator<CompanyTaxCalculatorConfig> testTaxCalculator = springTaxCalculatorFactory.getCalculator("testTaxCalculator");
        assertThat(testTaxCalculator).isNotNull();
    }

    @Test
    void getCalculator_unknownCalculator() {
        assertThatThrownBy(() -> springTaxCalculatorFactory.getCalculator("unknownCalculator"))
                .isInstanceOf(UnknownTaxCalculatorException.class);
    }

    @SpringBootApplication
    static class TestApplication {

        @Bean
        public TaxCalculator<PercentageTaxCalculatorConfig> testTaxCalculator() {
            return (request, config) -> BigDecimal.ONE;
        }

    }
}