package fr.gouv.economie.corporatetax.api.infrastructure.repository.yaml;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class YamlTaxCalculatorConfigRepositoryTest {

    private YamlTaxCalculatorConfigRepository repository = new YamlTaxCalculatorConfigRepository("calculators-config-test.yml");

    @Test
    void findConfigByCompanyType_present() {
        repository = new YamlTaxCalculatorConfigRepository("calculators-config-test.yml");

        assertThat(repository.findConfigByCompanyType("COMPANY_TYPE_1")).isPresent();
    }

    @Test
    void findConfigByCompanyType_empty() {
        assertThat(repository.findConfigByCompanyType("COMPANY_TYPE_ABSENT")).isEmpty();
    }

    @Test
    void findConfigByCompanyType_notFoundYamlFile() {
        YamlTaxCalculatorConfigRepository repository = new YamlTaxCalculatorConfigRepository("error.yml");

        assertThatThrownBy(() -> repository.findConfigByCompanyType("COMPANY_TYPE_ABSENT")).isInstanceOf(YamlConfigLoadingException.class);
    }

    @Test
    void findConfigByCompanyType_malformedYamlFile() {
        YamlTaxCalculatorConfigRepository repository = new YamlTaxCalculatorConfigRepository("calculators-config-test-malformed.yml");

        assertThatThrownBy(() -> repository.findConfigByCompanyType("COMPANY_TYPE_ABSENT")).isInstanceOf(YamlConfigLoadingException.class);
    }
}